package io.testproject.generated.tests.joannabusfirstproject;

import java.lang.Exception;
import org.openqa.selenium.WebDriver;

interface Test {
  void execute(WebDriver driver) throws Exception;
}
