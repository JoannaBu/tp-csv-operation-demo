package io.testproject.addon;

import io.testproject.sdk.internal.addons.ActionProxy;
import io.testproject.sdk.internal.addons.ProxyDescriptor;
import java.lang.String;

/**
 * Proxy for Parameters Operations Addon */
public class ParametersOperations {
  /**
   * Factory method for SetParameterValue */
  public static SetParameterValue getSetParameterValue() {
    return new SetParameterValue();
  }

  /**
   * Factory method for SetParameterValue
   * @param value The value to assign into a parameter */
  public static SetParameterValue setParameterValue(String value) {
    return new SetParameterValue(value);
  }

  /**
   * Set value to a parameter. Sets a value to a new or existing parameter */
  public static class SetParameterValue extends ActionProxy {
    /**
     * The value to assign into a parameter (INPUT) */
    public String value;

    /**
     * The output parameter that will be assigned with the value (OUTPUT) */
    public String outputParameter;

    public SetParameterValue() {
      this.setDescriptor(new ProxyDescriptor("aFWp2y4D5k6n8kKOGUMyDw", "SetParameterValue"));
    }

    public SetParameterValue(String value) {
      this();
      this.value = value;
    }
  }
}
