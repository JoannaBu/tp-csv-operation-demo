package io.testproject.addon;

import io.testproject.sdk.internal.addons.ActionProxy;
import io.testproject.sdk.internal.addons.ProxyDescriptor;
import java.lang.String;

/**
 * Proxy for CSV Operations Addon */
public class CSVOperations {
  /**
   * Factory method for AppendTextToCSV */
  public static AppendTextToCSV getAppendTextToCSV() {
    return new AppendTextToCSV();
  }

  /**
   * Factory method for AppendTextToCSV
   * @param values Values to add (values are separated by delimiter, and rows are separated by new line)
   * @param delimiter A single character to separate the values (e.g. comma, semi-colon, etc.)
   * @param path Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static AppendTextToCSV appendTextToCSV(String values, String delimiter, String path,
      String fileEncoding) {
    return new AppendTextToCSV(values,delimiter,path,fileEncoding);
  }

  /**
   * Factory method for CompareTwoCSV */
  public static CompareTwoCSV getCompareTwoCSV() {
    return new CompareTwoCSV();
  }

  /**
   * Factory method for CompareTwoCSV
   * @param pathToFirstCSV The path to the first CSV file
   * @param pathToSecondCSV The path to the second CSV file
   * @param pathToSaveDifferenceTextFile The path where to save the differences between the CSV files
   * @param nameOfFile The name of the file
   * @param fileEncoding1 First file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8)
   * @param fileEncoding2 Second file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static CompareTwoCSV compareTwoCSV(String pathToFirstCSV, String pathToSecondCSV,
      String pathToSaveDifferenceTextFile, String nameOfFile, String fileEncoding1,
      String fileEncoding2) {
    return new CompareTwoCSV(pathToFirstCSV,pathToSecondCSV,pathToSaveDifferenceTextFile,nameOfFile,fileEncoding1,fileEncoding2);
  }

  /**
   * Factory method for ConvertXLSXToCSV */
  public static ConvertXLSXToCSV getConvertXLSXToCSV() {
    return new ConvertXLSXToCSV();
  }

  /**
   * Factory method for ConvertXLSXToCSV
   * @param excelFile Path to the XLSX file (e.g. C:\Temp\file.xlsx)
   * @param newCSVFile Path where to store the new CSV file (e.g. C:\Temp\file.csv) */
  public static ConvertXLSXToCSV convertXLSXToCSV(String excelFile, String newCSVFile) {
    return new ConvertXLSXToCSV(excelFile,newCSVFile);
  }

  /**
   * Factory method for CreateCSV */
  public static CreateCSV getCreateCSV() {
    return new CreateCSV();
  }

  /**
   * Factory method for CreateCSV
   * @param names Columns names (separated by delimiter)
   * @param values Values (values are separated by delimiter, and rows are separated by new line)
   * @param delimiter A single character to separate the values (e.g. comma, semi-colon, etc.)
   * @param path Locally path to save the CSV file
   * @param fileName The name for the newly created CSV file */
  public static CreateCSV createCSV(String names, String values, String delimiter, String path,
      String fileName) {
    return new CreateCSV(names,values,delimiter,path,fileName);
  }

  /**
   * Factory method for CSVToJson */
  public static CSVToJson getCSVToJson() {
    return new CSVToJson();
  }

  /**
   * Factory method for CSVToJson
   * @param csvPath Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv)
   * @param fileName The name of the json file that will be stored on your local machine
   * @param jsonPath Local path to save the json file
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static CSVToJson cSVToJson(String csvPath, String fileName, String jsonPath,
      String fileEncoding) {
    return new CSVToJson(csvPath,fileName,jsonPath,fileEncoding);
  }

  /**
   * Factory method for DeleteColumnByIndex */
  public static DeleteColumnByIndex getDeleteColumnByIndex() {
    return new DeleteColumnByIndex();
  }

  /**
   * Factory method for DeleteColumnByIndex
   * @param path Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv)
   * @param index The index of the column to delete (starting from zero)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static DeleteColumnByIndex deleteColumnByIndex(String path, String index,
      String fileEncoding) {
    return new DeleteColumnByIndex(path,index,fileEncoding);
  }

  /**
   * Factory method for EditCSV */
  public static EditCSV getEditCSV() {
    return new EditCSV();
  }

  /**
   * Factory method for EditCSV
   * @param row Row in CSV (starting from one)
   * @param col Column in CSV (starting from one)
   * @param updateValue Value to update
   * @param fileToUpdate Path to the CSV file to update (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static EditCSV editCSV(int row, int col, String updateValue, String fileToUpdate,
      String fileEncoding) {
    return new EditCSV(row,col,updateValue,fileToUpdate,fileEncoding);
  }

  /**
   * Factory method for EditCSVCol */
  public static EditCSVCol getEditCSVCol() {
    return new EditCSVCol();
  }

  /**
   * Factory method for EditCSVCol
   * @param col Column in CSV (starting from one)
   * @param updateValue Value to update
   * @param fileToUpdate Path to the CSV file to update (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static EditCSVCol editCSVCol(int col, String updateValue, String fileToUpdate,
      String fileEncoding) {
    return new EditCSVCol(col,updateValue,fileToUpdate,fileEncoding);
  }

  /**
   * Factory method for EditCSVRow */
  public static EditCSVRow getEditCSVRow() {
    return new EditCSVRow();
  }

  /**
   * Factory method for EditCSVRow
   * @param row Row in CSV (starting from one)
   * @param updateValue Value to update
   * @param fileToUpdate Path to the CSV file to update (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static EditCSVRow editCSVRow(int row, String updateValue, String fileToUpdate,
      String fileEncoding) {
    return new EditCSVRow(row,updateValue,fileToUpdate,fileEncoding);
  }

  /**
   * Factory method for GetValueFromColAndRow */
  public static GetValueFromColAndRow getGetValueFromColAndRow() {
    return new GetValueFromColAndRow();
  }

  /**
   * Factory method for GetValueFromColAndRow
   * @param col Column in CSV (starting from one)
   * @param row Row in CSV (starting from one)
   * @param fileToRead Path to the CSV file to update (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static GetValueFromColAndRow getValueFromColAndRow(int col, int row, String fileToRead,
      String fileEncoding) {
    return new GetValueFromColAndRow(col,row,fileToRead,fileEncoding);
  }

  /**
   * Factory method for MergeTwoCSVFiles */
  public static MergeTwoCSVFiles getMergeTwoCSVFiles() {
    return new MergeTwoCSVFiles();
  }

  /**
   * Factory method for MergeTwoCSVFiles
   * @param oldFile Path to the old CSV file to append to (e.g. C:\Temp\old.csv)
   * @param newFile Path to the new CSV file to append (e.g. C:\Temp\new.csv)
   * @param fileEncoding1 First file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8)
   * @param fileEncoding2 Second file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static MergeTwoCSVFiles mergeTwoCSVFiles(String oldFile, String newFile,
      String fileEncoding1, String fileEncoding2) {
    return new MergeTwoCSVFiles(oldFile,newFile,fileEncoding1,fileEncoding2);
  }

  /**
   * Factory method for ReadColumn */
  public static ReadColumn getReadColumn() {
    return new ReadColumn();
  }

  /**
   * Factory method for ReadColumn
   * @param column Column in the CSV (starting from one)
   * @param fileToRead Path to the CSV file to read (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static ReadColumn readColumn(int column, String fileToRead, String fileEncoding) {
    return new ReadColumn(column,fileToRead,fileEncoding);
  }

  /**
   * Factory method for ReadRow */
  public static ReadRow getReadRow() {
    return new ReadRow();
  }

  /**
   * Factory method for ReadRow
   * @param row Row in the CSV (starting from one)
   * @param fileToRead Path to the CSV file to read (e.g. C:\Temp\new.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static ReadRow readRow(int row, String fileToRead, String fileEncoding) {
    return new ReadRow(row,fileToRead,fileEncoding);
  }

  /**
   * Factory method for ReverseCSVOrder */
  public static ReverseCSVOrder getReverseCSVOrder() {
    return new ReverseCSVOrder();
  }

  /**
   * Factory method for ReverseCSVOrder
   * @param path Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv)
   * @param fileEncoding File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) */
  public static ReverseCSVOrder reverseCSVOrder(String path, String fileEncoding) {
    return new ReverseCSVOrder(path,fileEncoding);
  }

  /**
   * Append values to a CSV file. Appends new values to an existing CSV file */
  public static class AppendTextToCSV extends ActionProxy {
    /**
     * Values to add (values are separated by delimiter, and rows are separated by new line) (INPUT) */
    public String values;

    /**
     * A single character to separate the values (e.g. comma, semi-colon, etc.) (INPUT) */
    public String delimiter;

    /**
     * Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv) (INPUT) */
    public String path;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public AppendTextToCSV() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.AppendTextToCSV"));
    }

    public AppendTextToCSV(String values, String delimiter, String path, String fileEncoding) {
      this();
      this.values = values;
      this.delimiter = delimiter;
      this.path = path;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Compare two CSV files.  */
  public static class CompareTwoCSV extends ActionProxy {
    /**
     * The path to the first CSV file (INPUT) */
    public String pathToFirstCSV;

    /**
     * The path to the second CSV file (INPUT) */
    public String pathToSecondCSV;

    /**
     * The path where to save the differences between the CSV files (INPUT) */
    public String pathToSaveDifferenceTextFile;

    /**
     * The name of the file (INPUT) */
    public String nameOfFile;

    /**
     * Result of comparison (OUTPUT) */
    public String resultOfComparison;

    /**
     * First file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding1;

    /**
     * Second file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding2;

    public CompareTwoCSV() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.CompareTwoCSV"));
    }

    public CompareTwoCSV(String pathToFirstCSV, String pathToSecondCSV,
        String pathToSaveDifferenceTextFile, String nameOfFile, String fileEncoding1,
        String fileEncoding2) {
      this();
      this.pathToFirstCSV = pathToFirstCSV;
      this.pathToSecondCSV = pathToSecondCSV;
      this.pathToSaveDifferenceTextFile = pathToSaveDifferenceTextFile;
      this.nameOfFile = nameOfFile;
      this.fileEncoding1 = fileEncoding1;
      this.fileEncoding2 = fileEncoding2;
    }
  }

  /**
   * Convert XLSX file to CSV. Converts an XLSX file to a CSV file */
  public static class ConvertXLSXToCSV extends ActionProxy {
    /**
     * Path to the XLSX file (e.g. C:\Temp\file.xlsx) (INPUT) */
    public String excelFile;

    /**
     * Path where to store the new CSV file (e.g. C:\Temp\file.csv) (INPUT) */
    public String newCSVFile;

    public ConvertXLSXToCSV() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.ConvertXLSXToCSV"));
    }

    public ConvertXLSXToCSV(String excelFile, String newCSVFile) {
      this();
      this.excelFile = excelFile;
      this.newCSVFile = newCSVFile;
    }
  }

  /**
   * Create a new CSV file. Creates a CSV file from a given string of values */
  public static class CreateCSV extends ActionProxy {
    /**
     * Columns names (separated by delimiter) (INPUT) */
    public String names;

    /**
     * Values (values are separated by delimiter, and rows are separated by new line) (INPUT) */
    public String values;

    /**
     * A single character to separate the values (e.g. comma, semi-colon, etc.) (INPUT) */
    public String delimiter;

    /**
     * Locally path to save the CSV file (INPUT) */
    public String path;

    /**
     * The name for the newly created CSV file (INPUT) */
    public String fileName;

    /**
     * Full path of the locally stored CSV file (OUTPUT) */
    public String result;

    public CreateCSV() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.CreateCSV"));
    }

    public CreateCSV(String names, String values, String delimiter, String path, String fileName) {
      this();
      this.names = names;
      this.values = values;
      this.delimiter = delimiter;
      this.path = path;
      this.fileName = fileName;
    }
  }

  /**
   * Export CSV file to json. Opens a CSV file and writing its content into a json string */
  public static class CSVToJson extends ActionProxy {
    /**
     * Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv) (INPUT) */
    public String csvPath;

    /**
     * The name of the json file that will be stored on your local machine (INPUT) */
    public String fileName;

    /**
     * Local path to save the json file (INPUT) */
    public String jsonPath;

    /**
     * Json string result (OUTPUT) */
    public String jsonString;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public CSVToJson() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.CSVToJson"));
    }

    public CSVToJson(String csvPath, String fileName, String jsonPath, String fileEncoding) {
      this();
      this.csvPath = csvPath;
      this.fileName = fileName;
      this.jsonPath = jsonPath;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Delete a column from a CSV file by index. Deletes a column from a CSV file by a specified index */
  public static class DeleteColumnByIndex extends ActionProxy {
    /**
     * Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv) (INPUT) */
    public String path;

    /**
     * The index of the column to delete (starting from zero) (INPUT) */
    public String index;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public DeleteColumnByIndex() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.DeleteColumnByIndex"));
    }

    public DeleteColumnByIndex(String path, String index, String fileEncoding) {
      this();
      this.path = path;
      this.index = index;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Update CSV file. Updates a CSV file at specified row and column */
  public static class EditCSV extends ActionProxy {
    /**
     * Row in CSV (starting from one) (INPUT) */
    public int row;

    /**
     * Column in CSV (starting from one) (INPUT) */
    public int col;

    /**
     * Value to update (INPUT) */
    public String updateValue;

    /**
     * Path to the CSV file to update (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToUpdate;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public EditCSV() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.EditCSV"));
    }

    public EditCSV(int row, int col, String updateValue, String fileToUpdate, String fileEncoding) {
      this();
      this.row = row;
      this.col = col;
      this.updateValue = updateValue;
      this.fileToUpdate = fileToUpdate;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Update CSV file entire column. Updates entire CSV file column */
  public static class EditCSVCol extends ActionProxy {
    /**
     * Column in CSV (starting from one) (INPUT) */
    public int col;

    /**
     * Value to update (INPUT) */
    public String updateValue;

    /**
     * Path to the CSV file to update (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToUpdate;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public EditCSVCol() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.EditCSVCol"));
    }

    public EditCSVCol(int col, String updateValue, String fileToUpdate, String fileEncoding) {
      this();
      this.col = col;
      this.updateValue = updateValue;
      this.fileToUpdate = fileToUpdate;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Update CSV file entire row. Updates entire CSV file row */
  public static class EditCSVRow extends ActionProxy {
    /**
     * Row in CSV (starting from one) (INPUT) */
    public int row;

    /**
     * Value to update (INPUT) */
    public String updateValue;

    /**
     * Path to the CSV file to update (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToUpdate;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public EditCSVRow() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.EditCSVRow"));
    }

    public EditCSVRow(int row, String updateValue, String fileToUpdate, String fileEncoding) {
      this();
      this.row = row;
      this.updateValue = updateValue;
      this.fileToUpdate = fileToUpdate;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Get value from a CSV file using specific row and column.  */
  public static class GetValueFromColAndRow extends ActionProxy {
    /**
     * Column in CSV (starting from one) (INPUT) */
    public int col;

    /**
     * Row in CSV (starting from one) (INPUT) */
    public int row;

    /**
     * Path to the CSV file to update (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToRead;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    /**
     * The value inside the cell (OUTPUT) */
    public String value;

    public GetValueFromColAndRow() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.GetValueFromColAndRow"));
    }

    public GetValueFromColAndRow(int col, int row, String fileToRead, String fileEncoding) {
      this();
      this.col = col;
      this.row = row;
      this.fileToRead = fileToRead;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Merge two CSV files. Merges a new CSV file to an old CSV file */
  public static class MergeTwoCSVFiles extends ActionProxy {
    /**
     * Path to the old CSV file to append to (e.g. C:\Temp\old.csv) (INPUT) */
    public String oldFile;

    /**
     * Path to the new CSV file to append (e.g. C:\Temp\new.csv) (INPUT) */
    public String newFile;

    /**
     * First file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding1;

    /**
     * Second file's encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding2;

    public MergeTwoCSVFiles() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.MergeTwoCSVFiles"));
    }

    public MergeTwoCSVFiles(String oldFile, String newFile, String fileEncoding1,
        String fileEncoding2) {
      this();
      this.oldFile = oldFile;
      this.newFile = newFile;
      this.fileEncoding1 = fileEncoding1;
      this.fileEncoding2 = fileEncoding2;
    }
  }

  /**
   * Read CSV column.  */
  public static class ReadColumn extends ActionProxy {
    /**
     * Column in the CSV (starting from one) (INPUT) */
    public int column;

    /**
     * Path to the CSV file to read (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToRead;

    /**
     * The value inside the cell (OUTPUT) */
    public String value;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public ReadColumn() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.ReadColumn"));
    }

    public ReadColumn(int column, String fileToRead, String fileEncoding) {
      this();
      this.column = column;
      this.fileToRead = fileToRead;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Read CSV row.  */
  public static class ReadRow extends ActionProxy {
    /**
     * Row in the CSV (starting from one) (INPUT) */
    public int row;

    /**
     * Path to the CSV file to read (e.g. C:\Temp\new.csv) (INPUT) */
    public String fileToRead;

    /**
     * The value inside the cell (OUTPUT) */
    public String value;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public ReadRow() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.ReadRow"));
    }

    public ReadRow(int row, String fileToRead, String fileEncoding) {
      this();
      this.row = row;
      this.fileToRead = fileToRead;
      this.fileEncoding = fileEncoding;
    }
  }

  /**
   * Reverse the order of a CSV file. Reveres the order of the provided CSV file */
  public static class ReverseCSVOrder extends ActionProxy {
    /**
     * Path to the locally stored CSV file (e.g. C:\Temp\myFile.csv) (INPUT) */
    public String path;

    /**
     * File encoding (Supports: UTF-8, UTF-16, UTF-16LE, UTF-16BE, US-ASCII, ISO-8859-1, Default: UTF-8) (INPUT) */
    public String fileEncoding;

    public ReverseCSVOrder() {
      this.setDescriptor(new ProxyDescriptor("5xTgapY7Q0mGOAy8eSnU5g", "Actions.ReverseCSVOrder"));
    }

    public ReverseCSVOrder(String path, String fileEncoding) {
      this();
      this.path = path;
      this.fileEncoding = fileEncoding;
    }
  }
}
