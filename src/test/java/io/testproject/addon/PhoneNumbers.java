package io.testproject.addon;

import io.testproject.sdk.internal.addons.ActionProxy;
import io.testproject.sdk.internal.addons.ProxyDescriptor;
import java.lang.String;

/**
 * Proxy for Phone Numbers Addon */
public class PhoneNumbers {
  /**
   * Factory method for RandomNumber */
  public static RandomNumber getRandomNumber() {
    return new RandomNumber();
  }

  /**
   * Factory method for RandomNumber
   * @param country Country name spelled in English or 'Random' to select country randomly */
  public static RandomNumber randomNumber(String country) {
    return new RandomNumber(country);
  }

  /**
   * Generate random phone number.  */
  public static class RandomNumber extends ActionProxy {
    /**
     * Country name spelled in English or 'Random' to select country randomly (INPUT) */
    public String country;

    /**
     *  (OUTPUT) */
    public String generatedNumber;

    /**
     *  (OUTPUT) */
    public String countryCode;

    /**
     *  (OUTPUT) */
    public String codelessNumber;

    public RandomNumber() {
      this.setDescriptor(new ProxyDescriptor("hbCjXt9A1EOt4rfQr3FEtw", "io.testproject.RandomNumber"));
    }

    public RandomNumber(String country) {
      this();
      this.country = country;
    }
  }
}
