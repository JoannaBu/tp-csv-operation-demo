package io.testproject.addon;

import io.testproject.sdk.internal.addons.ActionProxy;
import io.testproject.sdk.internal.addons.ProxyDescriptor;
import java.lang.String;

/**
 * Proxy for Generate Date-Time (Current, Future or Past) Addon */
public class GenerateDateTimeCurrentFutureorPast {
  /**
   * Factory method for FuturePastAction */
  public static FuturePastAction getFuturePastAction() {
    return new FuturePastAction();
  }

  /**
   * Factory method for FuturePastAction
   * @param days Add number to move in time (Example: -5 or 4)
   * @param months Add number to move in time (Example: -10 or 2)
   * @param years Add number to move in time (Example: -3 or 9)
   * @param hours Add number to move in time (Example: -2 or 7)
   * @param minutes Add number to move in time (Example: -1 or 6)
   * @param format Format (Default: dd/MM/yyyy, Example: HH:mm:ss.SSSZ) */
  public static FuturePastAction futurePastAction(int days, int months, int years, int hours,
      int minutes, String format) {
    return new FuturePastAction(days,months,years,hours,minutes,format);
  }

  /**
   * Get Future or Past Date.  */
  public static class FuturePastAction extends ActionProxy {
    /**
     * Add number to move in time (Example: -5 or 4) (INPUT) */
    public int days;

    /**
     * Add number to move in time (Example: -10 or 2) (INPUT) */
    public int months;

    /**
     * Add number to move in time (Example: -3 or 9) (INPUT) */
    public int years;

    /**
     * Add number to move in time (Example: -2 or 7) (INPUT) */
    public int hours;

    /**
     * Add number to move in time (Example: -1 or 6) (INPUT) */
    public int minutes;

    /**
     * Format (Default: dd/MM/yyyy, Example: HH:mm:ss.SSSZ) (INPUT) */
    public String format;

    /**
     * The resulting date (OUTPUT) */
    public String date;

    /**
     * Future Date in MilliSeconds(Epoch Time) (OUTPUT) */
    public long futureDateInMilliSeconds;

    public FuturePastAction() {
      this.setDescriptor(new ProxyDescriptor("02zAWIC7IUym1Rp8eFfaCg", "FuturePastAction"));
    }

    public FuturePastAction(int days, int months, int years, int hours, int minutes,
        String format) {
      this();
      this.days = days;
      this.months = months;
      this.years = years;
      this.hours = hours;
      this.minutes = minutes;
      this.format = format;
    }
  }
}
